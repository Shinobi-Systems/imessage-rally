$(document).ready(function(){
    const recentChatList = $('#recent-chat-list')
    const drawRecipientRow = (row) => {
        const html = `<div row-id="${row.ROWID}" data-recipient="${row.ROWID}">
            <div><b>${row.person_centric_id}</b></div>
            <div>id : ${row.id}</div>
            <div>country : ${row.country}</div>
            ${row.firstMessage && row.firstMessage.text ? `<div><small class="text-mute">${row.firstMessage.text}</small></div>` : ``}
        </div>`
        return html
    }
    const drawMessageRow = (row) => {
        const html = `<div>
            <div>handleId : ${row.handleId}</div>
            <div>callerId : ${row.callerId}</div>
            <div><i class="fa fa-${row.read ? `envelope-open` : `envelope`} text-${row.read ? `warning` : `default`}"></i></div>
        </div>`
        return html
    }
    const getMessagesForRecipient = (recipientId) => {
        $.get(`/getMessages?recipientId=${recipientId}`,function(data){
            console.log(data)
        })
    }
    $.get('/getRecipients',function(data){
        console.log(data)
        var html = ``
        data.rows.forEach((row) => {
            html += drawRecipientRow(row)
        })
        recentChatList.html(html)
    })
    $.get('/getMessages?limit=20',function(data){
        console.log('getMessages',data)
    })
    $(`body`)
        .on('click','[data-recipient]',function(el){
            const recipientId = $(this).attr('data-recipient')
            console.log($(this))
            getMessagesForRecipient(recipientId)
        })
})
