module.exports = function(s,config,io){
    //send data to socket client function
    s.tx = function(z,y,x){if(x){return x.broadcast.to(y).emit('f',z)};io.to(y).emit('f',z);}
    s.sendToConnectedUsers = function(data){
        return io.to('connectedUser').emit('f',data)
    }
    ////socket controller
    io.on('connection', function (cn) {
        s.debugLog('Client Connected', new Date(),cn.id)
        cn.join('connectedUser')
        cn.on('f',function(d){
            s.debugLog('Message from Client Panel',d)
        })
        cn.on('disconnect', function () {
            s.debugLog('Client Disconnected', new Date(),cn.id)
        })
    })
}
