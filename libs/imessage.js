const {
    getMessages,
    getMessagesFromId,
    getRecipients,
    getRecipientById,
    getAttachmentsFromId,
    sendFile,
    sendMessage,
    captureIncomingMessages,
} = require('./imessage/utils.js')
console.log(`This module uses a fork from https://github.com/elliotaplant/osa-imessage`)
module.exports = function(s,config,io,app){
    // imessage.sendFile('me@m03.ca', "/Users/snackbook/Desktop/Screen Shot 2020-10-07 at 11.22.51 AM.png")

    captureIncomingMessages((msg) => {
        console.log(msg)
        if (!msg.fromMe){
            io.emit('imessage',msg)
        }
    })


    app.get('/', async (req,res) => {
        const recipients = await getRecipients()
        s.renderPage(req,res,'pages/index')
    })

    app.get('/getRecipients', async (req,res) => {
        const recipients = await getRecipients()
        const rows = []
        const response = {
            ok: true,
        }
        var i;
        for (i = 0; i < recipients.rows.length; i++) {
            const row = recipients.rows[i]
            const firstMessage = (await getMessagesFromId(row.ROWID,null,1)).rows[0]
            row.firstMessage = firstMessage
        }
        response.rows = recipients.rows
        s.closeJsonResponse(res,response)
    })

    app.get('/getMessages', async (req,res) => {
        const recipientId = parseInt(req.query.recipientId)
        const searchQuery = req.query.search
        const limit = req.query.limit
        const response = recipientId ? (await getMessagesFromId(recipientId,searchQuery,limit)) : (await getMessages(searchQuery,null,limit))
        s.closeJsonResponse(res,response)
    })
}
