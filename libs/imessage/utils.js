const imessage = require('osa-imessage')
const iMessage = require('imessage')
const im = new iMessage();

const mapMessageArray = (rows) => {
    return rows.map((row) => {
        return {
            ROWID: row.ROWID,
            handleId: row.handle_id,
            read: row.read,
            guid: row.guid,
            text: row.text,
            callerId: row.destination_caller_id,
            date: row.date,
            is_sent: row.is_sent,
            is_audio_message: row.is_audio_message,
            is_played: row.is_played,
            date_played: row.date_played,
            // replyGuid: row.reply_to_guid,
        }
    })
}

const getMessages = (...args) => {
    const response = {
        ok: true
    }
    return new Promise((resolve,reject) => {
        const callback = (err,rows) => {
            if(err)response.err = err
            response.rows = mapMessageArray(rows)
            resolve(response)
        }
        if(args.length === 2){
            args.push('')
        }
        args.push(callback)
        im.getMessagesDESC(...args)
    })
}

const getMessagesFromId = (...args) => {
    const response = {
        ok: true
    }
    return new Promise((resolve,reject) => {
        const callback = (err,rows) => {
            if(err)response.err = err
            response.rows = mapMessageArray(rows)
            resolve(response)
        }
        if(args.length === 2){
            args.push('')
        }
        args.push(callback)
        im.getMessagesFromIdDESC(...args)
    })
}

const getRecipients = (...args) => {
    const response = {
        ok: true
    }
    return new Promise((resolve,reject) => {
        const callback = (err,rows) => {
            if(err)response.err = err
            response.rows = rows.filter(row => row.service === "iMessage")
            resolve(response)
        }
        args.push(callback)
        im.getRecipients(...args)
    })
}

const getRecipientById = (...args) => {
    const response = {
        ok: true
    }
    return new Promise((resolve,reject) => {
        const callback = (err,rows) => {
            if(err)response.err = err
            response.rows = rows
            resolve(response)
        }
        args.push(callback)
        im.getRecipientById(...args)
    })
}

const getAttachmentsFromId = (...args) => {
    const response = {
        ok: true
    }
    return new Promise((resolve,reject) => {
        const callback = (err,rows) => {
            if(err){
                response.ok = false
                response.err = err
            }
            response.rows = rows
            resolve(response)
        }
        args.push(callback)
        im.getAttachmentsFromId(...args)
    })
}

const sendFile = (...args) => {
    const response = {
        ok: true
    }
    return new Promise((resolve,reject) => {
        try{
            imessage.sendFile(...args)
        }catch(err){
            response.ok = false
        }
        resolve(response)
    })
}

const sendMessage = (...args) => {
    const response = {
        ok: true
    }
    return new Promise((resolve,reject) => {
        try{
            imessage.send(...args)
        }catch(err){
            response.ok = false
        }
        resolve(response)
    })
}

const captureIncomingMessages = (callback) => {
    imessage.listen().on('message', callback)
}

module.exports = {
    getMessages: getMessages,
    getMessagesFromId: getMessagesFromId,
    getRecipients: getRecipients,
    getRecipientById: getRecipientById,
    getAttachmentsFromId: getAttachmentsFromId,
    sendFile: sendFile,
    sendMessage: sendMessage,
    captureIncomingMessages: captureIncomingMessages,
}
