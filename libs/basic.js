var crypto = require('crypto');
var moment = require('moment');
module.exports = function(s,config){
    s.parseJSON = function(string){
        var parsed
        try{
            parsed = JSON.parse(string)
        }catch(err){

        }
        if(!parsed)parsed = string
        return parsed
    }
    s.stringJSON = function(json){
        try{
            if(json instanceof Object){
                json = JSON.stringify(json)
            }
        }catch(err){

        }
        return json
    }
    //JSON stringify short-hand
    s.s = JSON.stringify
    //Pretty Print JSON
    s.prettyPrint = function(obj){
        return JSON.stringify(obj,null,3)
    }
    s.gid = function(x){
        if(!x){x=10};var t = "";var p = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for( var i=0; i < x; i++ )
            t += p.charAt(Math.floor(Math.random() * p.length));
        return t;
    }
    s.md5 = function(x){return crypto.createHash('md5').update(x).digest("hex")}
    s.createHash = s.md5
    s.timeObject = moment
    s.debugLog = function(q,w,e){
        if(config.debugLog === true){
            if(!w){w = ''}
            if(!e){e = ''}
            console.log(s.timeObject().format(),q,w,e)
            if(config.debugLogVerbose === true){
                console.log(new Error())
            }
        }
    }
    s.checkCorrectPathEnding = function(x){
        var length=x.length
        if(x.charAt(length-1)!=='/'){
            x=x+'/'
        }
        return x.replace('__DIR__',s.mainDirectory)
    }
    s.addToObject = function(obj,newObjData){
        Object.keys(newObjData).forEach(function(key){
            obj[key] = newObjData[key]
        })
    }
    s.splitForFFPMEG = function (ffmpegCommandAsString) {
        //this function ignores spaces inside quotes.
        return ffmpegCommandAsString.match(/\\?.|^$/g).reduce((p, c) => {
            if(c === '"'){
                p.quote ^= 1;
            }else if(!p.quote && c === ' '){
                p.a.push('');
            }else{
                p.a[p.a.length-1] += c.replace(/\\(.)/,"$1");
            }
            return  p;
        }, {a: ['']}).a
    }
    function isObject(item) {
      return (item && typeof item === 'object' && !Array.isArray(item));
    }
    const mergeDeep = (target, ...sources) => {
      if (!sources.length) return target;
      const source = sources.shift();

      if (isObject(target) && isObject(source)) {
        for (const key in source) {
          if (isObject(source[key])) {
            if (!target[key]) Object.assign(target, { [key]: {} });
            mergeDeep(target[key], source[key]);
          } else {
            Object.assign(target, { [key]: source[key] });
          }
        }
      }

      return mergeDeep(target, ...sources);
    }
    s.mergeDeep = mergeDeep
}
